<?php


namespace Src;


class Node
{
    /** @var int */
    protected $data;

    /** @var Node|null */
    protected $right;

    /** @var Node|null */
    protected $left;

    public function __construct(int $data, Node $left = null, Node $right = null)
    {
        $this->data = $data;
        $this->right = $right;
        $this->left = $left;
    }

    /** @param Node|null $left */
    public function setLeft(?Node $left): void
    {
        $this->left = $left;
    }

    /** @return Node|null */
    public function getLeft(): ?Node
    {
        return $this->left;
    }

    /** @return Node|null */
    public function getRight(): ?Node
    {
        return $this->right;
    }

    /** @param Node|null $right */
    public function setRight(?Node $right): void
    {
        $this->right = $right;
    }

    /** @return int */
    public function getData(): int
    {
        return $this->data;
    }
}
