<?php


namespace Src;


class BinaryTree implements BinaryTreeInterface
{
    /** @var Node|null */
    protected $root;

    /** @var int */
    protected $count = 0;


    /**
     * @param int $data
     */
    public function add(int $data): void
    {
        $this->addTree($data, $this->root);
    }

    /**
     * @param int $data
     */
    public function remove(int $data): void
    {
        if ($this->root === null) {
            return;
        }

        if ($this->find($data)) {

            $this->count--;

            $parent = $this->searchParent($this->root, $data);

            if ($parent === null) {
                $newTree = $this->root->getRight();
                $newRoot = $this->root->getLeft();
                unset($this->root);
                $this->root = $newRoot;
                if ($newTree != null) {
                    $this->unsetAndAddElement($newTree);
                }
            } else {
                $newTree = $parent->getData() > $data ? $parent->getLeft() : $parent->getRight();
                $parent->getData() > $data ? $parent->setLeft(null) : $parent->setRight(null);
                if ($newTree != null) {
                    $this->unsetAndAddElement($newTree->getRight());
                    $this->unsetAndAddElement($newTree->getLeft());
                }
            }
        }
    }

    /**
     * @param int $data
     * @return bool
     */
    public function find(int $data): bool
    {
        return $this->findElement($data, $this->root);
    }

    /**
     * @return int|null
     */
    public function findMin(): ?int
    {
        return $this->min($this->root);
    }

    /**
     * @return int|null
     */
    public function findMax(): ?int
    {
        return $this->max($this->root);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->empty();
    }

    /**
     * @return int
     */
    public function elementCount(): int
    {
        return $this->count;
    }

    /**
     * @return string
     */
    public function detour(): string
    {
        return $this->exit($this->root);
    }

    protected function addTree(int $data, ?Node $node): void
    {
        if ($node === null) {
            $this->root = new Node($data);
            $this->count++;
            return;
        }

        if ($data > $node->getData()) {
            if ($node->getRight() === null) {
                $node->setRight(new Node($data));
                $this->count++;
            } else {
                $this->addTree($data, $node->getRight());
            }
        }
        if ($data < $node->getData()) {
            if ($node->getLeft() === null) {
                $node->setLeft(new Node($data));
                $this->count++;
            } else {
                $this->addTree($data, $node->getLeft());
            }
        }

    }

    protected function findElement(int $data, ?Node $node): bool
    {
        if ($node === null) {
            return false;
        }
        if ($node->getData() === $data) {
            return true;
        }

        $nextNode = $node->getData() > $data ? $node->getLeft() : $node->getRight();
        return $this->findElement($data, $nextNode);
    }

    protected function empty(): bool
    {
        if ($this->root) {
            return false;
        } else {
            return true;
        }
    }

    protected function max(?Node $node): ?int
    {
        if ($this->isEmpty()) {
            return null;
        }
        if ($node->getRight() === null) {
            return $node->getData();
        } else {
            return $this->max($node->getRight());
        }
    }

    protected function min(?Node $node): ?int
    {
        if ($this->isEmpty()) {
            return null;
        }
        if ($node->getLeft() === null) {
            return $node->getData();
        } else {
            return $this->min($node->getLeft());
        }
    }

    protected function exit(?Node $node, int $level = 1): string
    {
        if ($node === null) {
            return '';
        }
        $result = $this->exit($node->getRight(), $level + 1);
        $n = $level;
        while ($n--) {
            $result .= "\t";
        }
        $result .= $node->getData() . PHP_EOL;
        $result .= $this->exit($node->getLeft(), $level + 1);

        return $result;
    }

    protected function searchParent(?Node $node, int $data): ?Node
    {
        if ($node === null) {
            return null;
        }

        if (
            ($node->getRight() && $node->getRight()->getData() === $data) ||
            ($node->getLeft() && $node->getLeft()->getData() === $data)) {
            return $node;
        }
        if ($node->getData() < $data) {
            return $this->searchParent($node->getRight(), $data);
        } else {
            return $this->searchParent($node->getLeft(), $data);
        }
    }

    protected function unsetAndAddElement(?Node $node)
    {
        if ($node === null) {
            return null;
        }

        $this->unsetAndAddElement($node->getRight());
        $this->add($node->getData());
        $this->unsetAndAddElement($node->getLeft());

        unset($node);
    }
}
