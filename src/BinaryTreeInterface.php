<?php

namespace Src;

interface BinaryTreeInterface
{
    public function add(int $data): void;

    public function remove(int $data): void;

    public function find(int $data): bool;

    public function findMin(): ?int;

    public function findMax(): ?int;

    public function isEmpty(): bool;

    public function elementCount(): int;

    public function detour(): string;
}
