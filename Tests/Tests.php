<?php



namespace Tests;


use PHPUnit\Framework\TestCase;
use Src\BinaryTree;


class Tests extends TestCase
{
    /**
     * @var BinaryTree
     */
    protected $tree;

    protected function setUp()
    {
        $this->createTree();
        parent::setUp();
    }

    public function testAddAndIsEmpty()
    {
        $this->assertTrue($this->tree->isEmpty());
        $this->tree->add(1);
        $this->assertFalse($this->tree->isEmpty());
        $this->tree->remove(1);
        $this->assertTrue($this->tree->isEmpty());
    }

    public function testFindNotExistingElement()
    {
        $this->tree->add(1);
        $this->assertFalse($this->tree->find(2));
    }

    public function testFindExistingElement()
    {
        $this->tree->add(1);
        $this->assertTrue($this->tree->find(1));
    }

    public function testFindEmptyTree()
    {
        $this->assertFalse($this->tree->find(1));
    }

    public function testFindMaxElement()
    {
        $this->tree->add(1);
        $this->tree->add(2);
        $this->assertEquals(2, $this->tree->findMax());
    }

    public function testFindMaxEmptyTree()
    {
        $this->assertEquals(null, $this->tree->findMax());
    }

    public function testFindMinElement()
    {
        $this->tree->add(1);
        $this->tree->add(2);
        $this->assertEquals(1, $this->tree->findMin());
    }

    public function testFindMinEmptyTree()
    {
        $this->assertEquals(null, $this->tree->findMin());
    }

    public function testCountEmptyTree()
    {
        $this->assertEquals(0, $this->tree->elementCount());
    }

    public function testCountNotEmptyTree()
    {
        $this->tree->add(1);
        $this->assertEquals(1, $this->tree->elementCount());
        $this->tree->add(1);
        $this->assertEquals(1, $this->tree->elementCount());
    }

    public function testRemoveExistingElementAndRoot()
    {
        $this->tree->add(2);
        $this->tree->add(1);
        $this->tree->add(3);
        $this->tree->remove(1);
        $this->assertEquals(2, $this->tree->elementCount());
        $this->tree->remove(3);
        $this->assertEquals(1, $this->tree->elementCount());
        $this->tree->remove(2);
        $this->assertEquals(true, $this->tree->isEmpty());
        $this->assertEquals(0, $this->tree->elementCount());
        $this->tree->remove(2);
        $this->assertEquals(true, $this->tree->isEmpty());
        $this->assertEquals(0, $this->tree->elementCount());
    }

    public function testRemoveNotExistingElement()
    {
        $this->tree->add(2);
        $this->tree->add(1);
        $this->tree->remove(4);
        $this->assertEquals(2, $this->tree->elementCount());


    }

    public function testDetour()
    {
        $this->tree->add(2);
//        $this->tree->add(1);
//        $this->tree->add(3);
        $this->assertEquals("\t" . "2" . PHP_EOL, $this->tree->detour());

    }

    protected function createTree()
    {
        $this->tree = new BinaryTree();
    }
}
